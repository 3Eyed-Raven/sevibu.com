@extends('layouts.frontend')

@section('title', 'Ivory Property')

@section('content')

<div class="widget widget-recentproperties">
  <div class="widget-content properties-grid">
      <div class="row">
          <div class="col-md-4 col-sm-6">
              <div class="thumbnail thumbnail-property features">
                  <div class="property-image">
                      <img src="frontend/img/placeholders/400x300.png" alt="car" />
                      <div class="image-count">
                          <i class="icon-image"></i>
                          <span>2</span>
                      </div>
                      <div class="budget budget-used">
                          <div class="budget-mask"><span>Used</span></div>
                      </div>
                      <a href="listing.html" class="property-image-hover">
                          <span class="property-im-m property-im-m-lt"></span>
                          <span class="property-im-m property-im-m-lb"></span>
                          <span class="property-im-m property-im-m-rt"></span>
                          <span class="property-im-m property-im-m-rb"></span>
                      </a>
                  </div>
                  <div class="caption">
                      <div class="anchor">
                          <a href="#" class=""><i class="fa fa-bookmark"></i>
                          </a>
                      </div>
                      <h3 class="property-title"><a href="listing.html">Fast car 1,6 TDI DSG</a></h3>
                      <p class="property-description">2014. g. 39.138 km, 66kW/90KS</p>
                      <span class="property-field">114.630 km</span>
                      <div class="property-ratings">
                          <i class="icon-star-ratings-1"></i>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-6">
              <div class="thumbnail thumbnail-property features">
                  <div class="property-image">
                      <img src="frontend/img/placeholders/400x300.png" alt="car" />
                      <div class="image-count">
                          <i class="icon-image"></i>
                          <span>2</span>
                      </div>
                      <div class="budget budget-used">
                          <div class="budget-mask"><span>Used</span></div>
                      </div>
                      <a href="listing.html" class="property-image-hover">
                          <span class="property-im-m property-im-m-lt"></span>
                          <span class="property-im-m property-im-m-lb"></span>
                          <span class="property-im-m property-im-m-rt"></span>
                          <span class="property-im-m property-im-m-rb"></span>
                      </a>
                  </div>
                  <div class="caption">
                      <div class="anchor active">
                          <a href="#" class=""><i class="fa fa-bookmark"></i>
                          </a>
                      </div>
                      <h3 class="property-title"><a href="listing.html">Blue car  1,6 TDI DSG</a></h3>
                      <p class="property-description">2012. g. 39.138 km, 70kW/80KS</p>
                      <span class="property-field">204.630 km</span>
                      <div class="property-ratings">
                          <i class="icon-star-ratings-3"></i>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-6">
              <div class="thumbnail thumbnail-property features">
                  <div class="property-image">
                      <img src="frontend/img/placeholders/400x300.png" alt="car" />
                      <div class="image-count">
                          <i class="icon-image"></i>
                          <span>2</span>
                      </div>
                      <div class="budget budget-damage">
                          <div class="budget-mask"><span>Damaged</span></div>
                      </div>
                      <a href="listing.html" class="property-image-hover">
                          <span class="property-im-m property-im-m-lt"></span>
                          <span class="property-im-m property-im-m-lb"></span>
                          <span class="property-im-m property-im-m-rt"></span>
                          <span class="property-im-m property-im-m-rb"></span>
                      </a>
                  </div>
                  <div class="caption">
                      <div class="anchor">
                          <a href="#" class=""><i class="fa fa-bookmark"></i>
                          </a>
                      </div>
                      <h3 class="property-title"><a href="listing.html">Blue car A1  3,5 TDI DSG</a></h3>
                      <p class="property-description">2015.g. 19.13 km, 68GmbH/70</p>
                      <span class="property-field">14.630 km</span>
                      <div class="property-ratings">
                          <i class="icon-star-ratings-2"></i>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-6">
              <div class="thumbnail thumbnail-property">
                  <div class="property-image">
                      <img src="frontend/img/placeholders/400x300.png" alt="car" />
                      <div class="image-count">
                          <i class="icon-image"></i>
                          <span>2</span>
                      </div>
                      <a href="listing.html" class="property-image-hover">
                          <span class="property-im-m property-im-m-lt"></span>
                          <span class="property-im-m property-im-m-lb"></span>
                          <span class="property-im-m property-im-m-rt"></span>
                          <span class="property-im-m property-im-m-rb"></span>
                      </a>
                  </div>
                  <div class="caption">
                      <div class="anchor">
                          <a href="#" class=""><i class="fa fa-bookmark"></i>
                          </a>
                      </div>
                      <h3 class="property-title"><a href="listing.html">Fast car A6 4,6 TDI DSG</a></h3>
                      <p class="property-description">2015.g. 19.00 km, 68GmbH/70</p>
                      <span class="property-field">24.000 km</span>
                      <div class="property-ratings">
                          <i class="icon-star-ratings-5"></i>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-6">
              <div class="thumbnail thumbnail-property">
                  <div class="property-image">
                      <img src="frontend/img/placeholders/400x300.png" alt="car" />
                      <div class="image-count">
                          <i class="icon-image"></i>
                          <span>1</span>
                      </div>
                      <a href="listing.html" class="property-image-hover">
                          <span class="property-im-m property-im-m-lt"></span>
                          <span class="property-im-m property-im-m-lb"></span>
                          <span class="property-im-m property-im-m-rt"></span>
                          <span class="property-im-m property-im-m-rb"></span>
                      </a>
                  </div>
                  <div class="caption">
                      <div class="anchor">
                          <a href="#" class=""><i class="fa fa-bookmark"></i>
                          </a>
                      </div>
                      <h3 class="property-title"><a href="listing.html">Red sedan  2,0 TDI DSG</a></h3>
                      <p class="property-description">2014. g. 49.138 km, 56kW/70KS</p>
                      <span class="property-field">100.600 km</span>
                      <div class="property-ratings">
                          <i class="icon-star-ratings"></i>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-6">
              <div class="thumbnail thumbnail-property">
                  <div class="property-image">
                      <img src="frontend/img/placeholders/400x300.png" alt="car" />
                      <div class="image-count">
                          <i class="icon-image"></i>
                          <span>5</span>
                      </div>
                      <a href="listing.html" class="property-image-hover">
                          <span class="property-im-m property-im-m-lt"></span>
                          <span class="property-im-m property-im-m-lb"></span>
                          <span class="property-im-m property-im-m-rt"></span>
                          <span class="property-im-m property-im-m-rb"></span>
                      </a>
                  </div>
                  <div class="caption">
                      <div class="anchor">
                          <a href="#" class=""><i class="fa fa-bookmark"></i>
                          </a>
                      </div>
                      <h3 class="property-title"><a href="listing.html">Datia  1,4 TDI DSG</a></h3>
                      <p class="property-description">2009.g. 12.140 km, 75kW/80KS</p>
                      <span class="property-field">10.004 km</span>
                      <div class="property-ratings">
                          <i class="icon-star-ratings-2"></i>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-6">
              <div class="thumbnail thumbnail-property">
                  <div class="property-image">
                      <img src="frontend/img/placeholders/400x300.png" alt="car" />
                      <div class="image-count">
                          <i class="icon-image"></i>
                          <span>2</span>
                      </div>
                      <a href="listing.html" class="property-image-hover">
                          <span class="property-im-m property-im-m-lt"></span>
                          <span class="property-im-m property-im-m-lb"></span>
                          <span class="property-im-m property-im-m-rt"></span>
                          <span class="property-im-m property-im-m-rb"></span>
                      </a>
                  </div>
                  <div class="caption">
                      <div class="anchor">
                          <a href="#" class=""><i class="fa fa-bookmark"></i>
                          </a>
                      </div>
                      <h3 class="property-title"><a href="listing.html">Classic sedan 1,4 TDI</a></h3>
                      <p class="property-description">2010. g. 120.14 km, 75kW/80KS</p>
                      <span class="property-field">160.004 km</span>
                      <div class="property-ratings">
                          <i class="icon-star-ratings-2"></i>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-6">
              <div class="thumbnail thumbnail-property">
                  <div class="property-image">
                      <img src="frontend/img/placeholders/400x300.png" alt="car" />
                      <div class="image-count">
                          <i class="icon-image"></i>
                          <span>2</span>
                      </div>
                      <a href="listing.html" class="property-image-hover">
                          <span class="property-im-m property-im-m-lt"></span>
                          <span class="property-im-m property-im-m-lb"></span>
                          <span class="property-im-m property-im-m-rt"></span>
                          <span class="property-im-m property-im-m-rb"></span>
                      </a>
                  </div>
                  <div class="caption">
                      <div class="anchor">
                          <a href="#" class=""><i class="fa fa-bookmark"></i>
                          </a>
                      </div>
                      <h3 class="property-title"><a href="listing.html">Modern car  1,6 TDI DSG</a></h3>
                      <p class="property-description">2014. g. 39.138 km, 66kW/90KS</p>
                      <span class="property-field">114.630 km</span>
                      <div class="property-ratings">
                          <i class="icon-star-ratings-3"></i>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-6">
              <div class="thumbnail thumbnail-property">
                  <div class="property-image">
                      <img src="frontend/img/placeholders/400x300.png" alt="car" />
                      <div class="image-count">
                          <i class="icon-image"></i>
                          <span>2</span>
                      </div>
                      <a href="listing.html" class="property-image-hover">
                          <span class="property-im-m property-im-m-lt"></span>
                          <span class="property-im-m property-im-m-lb"></span>
                          <span class="property-im-m property-im-m-rt"></span>
                          <span class="property-im-m property-im-m-rb"></span>
                      </a>
                  </div>
                  <div class="caption">
                      <div class="anchor">
                          <a href="#" class=""><i class="fa fa-bookmark"></i>
                          </a>
                      </div>
                      <h3 class="property-title"><a href="listing.html">Hybrid car TDI DSG</a></h3>
                      <p class="property-description">2013. g. 30.130 km, 60kW/90KS</p>
                      <span class="property-field">37.630 km</span>
                      <div class="property-ratings">
                          <i class="icon-star-ratings-3"></i>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-6">
              <div class="thumbnail thumbnail-property">
                  <div class="property-image">
                      <img src="frontend/img/placeholders/400x300.png" alt="car" />
                      <div class="image-count">
                          <i class="icon-image"></i>
                          <span>2</span>
                      </div>
                      <a href="listing.html" class="property-image-hover">
                          <span class="property-im-m property-im-m-lt"></span>
                          <span class="property-im-m property-im-m-lb"></span>
                          <span class="property-im-m property-im-m-rt"></span>
                          <span class="property-im-m property-im-m-rb"></span>
                      </a>
                  </div>
                  <div class="caption">
                      <div class="anchor">
                          <a href="#" class=""><i class="fa fa-bookmark"></i>
                          </a>
                      </div>
                      <h3 class="property-title"><a href="listing.html">Infinity car 3,2 TDI DSG</a></h3>
                      <p class="property-description">2012. g. 50.130 km, 70kW/90KS</p>
                      <span class="property-field">457.400 km</span>
                      <div class="property-ratings">
                          <i class="icon-star-ratings-3"></i>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-6">
              <div class="thumbnail thumbnail-property">
                  <div class="property-image">
                      <img src="frontend/img/placeholders/400x300.png" alt="car" />
                      <div class="image-count">
                          <i class="icon-image"></i>
                          <span>2</span>
                      </div>
                      <a href="listing.html" class="property-image-hover">
                          <span class="property-im-m property-im-m-lt"></span>
                          <span class="property-im-m property-im-m-lb"></span>
                          <span class="property-im-m property-im-m-rt"></span>
                          <span class="property-im-m property-im-m-rb"></span>
                      </a>
                  </div>
                  <div class="caption">
                      <div class="anchor">
                          <a href="#" class=""><i class="fa fa-bookmark"></i>
                          </a>
                      </div>
                      <h3 class="property-title"><a href="listing.html">Fast car 2,0 TDI DSG</a></h3>
                      <p class="property-description">2009. g. 30.130 km, 95kW/80KS</p>
                      <span class="property-field">123.470 km</span>
                      <div class="property-ratings">
                          <i class="icon-star-ratings-5"></i>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-6">
              <div class="thumbnail thumbnail-property">
                  <div class="property-image">
                      <img src="frontend/img/placeholders/400x300.png" alt="car" />
                      <div class="image-count">
                          <i class="icon-image"></i>
                          <span>2</span>
                      </div>
                      <a href="listing.html" class="property-image-hover">
                          <span class="property-im-m property-im-m-lt"></span>
                          <span class="property-im-m property-im-m-lb"></span>
                          <span class="property-im-m property-im-m-rt"></span>
                          <span class="property-im-m property-im-m-rb"></span>
                      </a>
                  </div>
                  <div class="caption">
                      <div class="anchor">
                          <a href="#" class=""><i class="fa fa-bookmark"></i>
                          </a>
                      </div>
                      <h3 class="property-title"><a href="listing.html">Alfa 2, TDI</a></h3>
                      <p class="property-description">2014. g. 31.130 km, 70kW/90KS</p>
                      <span class="property-field">25.630 km</span>
                      <div class="property-ratings">
                          <i class="icon-star-ratings-4"></i>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <nav class="text-center" aria-label="Page navigation">
          <ul class="pagination">
              <li>
                  <a href="#" aria-label="Previous">
                      <span aria-hidden="true">&laquo;</span>
                  </a>
              </li>
              <li><a href="#" class="active">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li>
                  <a href="#" aria-label="Next">
                      <span aria-hidden="true">&raquo;</span>
                  </a>
              </li>
          </ul>
      </nav>
  </div> <!-- /. recent properties -->
</div>
<aside class="widget widget-ads visible-xs-block">
  <div class="widget-header vert-line-r-l vert-line-primary">
      <h2>Sponsor</h2>
  </div>
  <div class="widget-content">
      <a href="#"><img src="frontend/img/placeholders/170x140.png" alt="" /></a>
  </div>
</aside> <!-- /. ads -->
<div class="widget widgt-agents visible-xs-block">
  <div class="widget-header vert-line-r-l vert-line-primary">
      <h2>Dealers</h2>
  </div>
  <div class="widget-content">
      <div class="agent-box media">
          <div class="media-left">
              <a href="page_profile.html" class="agent-box-image image-hoveffect-zoom"><img src="frontend/img/placeholders/125x125.png" alt="" /></a>
          </div>
          <div class="media-right">
              <h3 class="title"><a href="page_profile.html" class="primary-hover">Ketty Springs (6)</a></h3>
              <span class="number">
                  +385 (0)91 123 321
              </span>
              <span class="mail"><a href="mailto:kety@website.com">kety@website.com</a></span>
          </div>
      </div>
      <div class="agent-box media">
          <div class="media-left">
              <a href="page_profile.html" class="agent-box-image image-hoveffect-zoom"><img src="frontend/img/placeholders/125x125.png" alt="" /></a>
          </div>
          <div class="media-right">
              <h3 class="title"><a href="page_profile.html" class="primary-hover">Pero Peric (4)</a></h3>
              <span class="number">
                  +385 (0)91 123 321
              </span>
              <span class="mail"><a href="mailto:kety@website.com">pero@website.com</a></span>
          </div>
      </div>
      <div class="agent-box media">
          <div class="media-left">
              <a href="page_profile.html" class="agent-box-image image-hoveffect-zoom"><img src="frontend/img/placeholders/125x125.png" alt="" /></a>
          </div>
          <div class="media-right">
              <h3 class="title"><a href="page_profile.html" class="primary-hover">Lazo Biskup (3)</a></h3>
              <span class="number">
                  +385 (0)91 123 321
              </span>
              <span class="mail"><a href="mailto:kety@website.com">lazo@website.com</a></span>
          </div>
      </div>
  </div>
</div> <!-- /. agents-list -->

@endsection