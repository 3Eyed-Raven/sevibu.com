<div class="footer-content box-fill">
  <div class="container">
    <div class="row">
      <div class="custom-box box-quicle-menu col-md-3 col-sm-6">
        <div class="custom-box-title vert-line-r-l vert-line-primary">
          <h2>Ivory Property</h2>
        </div>
        <div class="custom-box-content">
          <ul class="list">
            <li><a href="index.html">Home</a></li>
            <li><a href="slider_standard.html">Search</a></li>
            <li><a href="page_agents.html">Dealers</a></li>
            <li><a href="page_featured.html">Featuread Pages</a></li>
            <li><a href="page_contact.html">Contact</a></li>
          </ul>
        </div>
      </div>
      <div class="custom-box box-contact col-md-3 col-sm-6">
        <div class="custom-box-title vert-line-r-l vert-line-primary">
          <h2>Contact</h2>
        </div>
        <div class="custom-box-content">
          <p>
            World Dealer
            <br/> Ilica 345
            <br/> HR-10000 Zagreb
            <br/> Phone: +385 (0)1 123 321
            <br/> Fax: +385 (0)1 123 322
            <br/> Email: info@my-website.com
            <br/>
          </p>
        </div>
      </div>
      <div class="custom-box box-social col-md-3 col-sm-6">
        <div class="custom-box-title vert-line-r-l vert-line-primary">
          <h2>Social</h2>
        </div>
        <div class="custom-box-content">
          <div id="fb-root"></div>
          <div class="sidebar-ads-1 widget-content">
            <div class="fb-page" data-href="https://www.facebook.com/facebook" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false">
              <div class="fb-xfbml-parse-ignore">
                <blockquote cite="https://www.facebook.com/facebook"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="custom-box box-about col-md-3 col-sm-6">
        <div class="custom-box-title vert-line-r-l vert-line-primary">
          <h2>About</h2>
        </div>
        <div class="custom-box-content">
          <p>
            World Dealer MultiPurpose Script is a multi-solution product made with simplicity in mind so you can benefit as much as possible from it.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="footer-bottom">
  <div class="container">
    <div class="row">
    <div class="col-sm-6 text-left">All Right reserved &#169; {{ date('Y') }}</div>
    </div>
  </div>
</div>