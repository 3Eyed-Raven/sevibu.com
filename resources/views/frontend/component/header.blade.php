<div class="top-bar color-primary">
  <div class="<!--container-->">
    <div class="top-bar-left">
      <div class="top-bar-body">
        <a href="index.html" class="logo">
          <img src="frontend/img/placeholders/60x60.png" alt="" />
        </a>
        <h1 class="website-title"><a href="index.html" data-hover="Ivory Property">Ivory Property</a></h1>
      </div>
      <div class="skew-block"></div>
    </div>
      
    <div class="top-bar-right search-form-middle-phone hidden">
      <div class="skew-block"></div>
      <div class="search-form-mini closed">
        <a href="#" class="search"><i class="fa fa-search"></i></a>
        <input type="text" placeholder="Search" />
        <a href="#" class="close"><i class="fa fa-remove"></i></a>
      </div>
      <a href="#" class="search-btn search-form-mini-open"></a>
    </div>
    <div class="search-form-mini-phone hidden-sm">
      <div>
        <a href="#" class="search"><i class="fa fa-search"></i></a>
        <input type="text" placeholder="Search" />
        <a href="#" class="close"><i class="fa fa-remove"></i></a>
      </div>
    </div>
    <div class="top-bar-center">
      <nav class="navbar mainmenu">
        <div class="container-fluid">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              {{-- <a class="dropdown-toggle" data-toggle="dropdown" href="#"> Slider
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li><a href="slider_standard.html">Standard</a></li>
                <li><a href="homepage_video.html">Video</a></li>
                <li><a href="slider_full-screen.html">Full Screen</a></li>
              </ul> --}}
            </li>
          </ul>
        </div>
      </nav><!-- /.main menu -->
    </div>
    <div class="top-bar-right">
      <div class="skew-block color-secondary"></div>
      <div class="search-form-mini closed">
        <a href="#" class="search"><i class="fa fa-search"></i></a>
        <input type="text" placeholder="Search" />
        <a href="#" class="close"><i class="fa fa-remove"></i></a>
      </div>
      <a href="#" class="search-btn color-secondary search-form-mini-open"></a>
    </div>
  </div>
</div>