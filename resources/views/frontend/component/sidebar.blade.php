<div class="widget-header vert-line-r-l vert-line-primary" style="color: #fff; padding-bottom: 20px;">
  <h2>Search</h2>
</div>
<div class="widget-content">
  <form action="#">
    <div class="form-group">
      <select class="form-control selectpicker">
        <option value="">Tipe Properti</option>
        <option value="a"></option>
        <option value="B">B</option>
        <option value="c">C</option>
        <option value="d">D</option>
        <option value="e">E</option>
        <option value="f">F</option>
      </select>
    </div>
    <div class="form-group">
      <select class="form-control selectpicker">
        <option value="">Lokasi :</option>
        <option value="a"></option>
        <option value="B">B</option>
        <option value="c">C</option>
        <option value="d">D</option>
        <option value="e">E</option>
        <option value="f">F</option>
      </select>
    </div>
    <div class="form-group">
      <select class="form-control selectpicker">
        <option value="">Kota</option>
        <option value="a"></option>
        <option value="B">B</option>
        <option value="c">C</option>
        <option value="d">D</option>
        <option value="e">E</option>
        <option value="f">F</option>
      </select>
    </div>
    <div class="form-group">
      <select class="form-control selectpicker">
        <option value="">Kecamatan</option>
        <option value="a"></option>
        <option value="B">B</option>
        <option value="c">C</option>
        <option value="d">D</option>
        <option value="e">E</option>
        <option value="f">F</option>
      </select>
    </div>
    <div class="form-group">
      <select class="form-control selectpicker">
        <option value="">Kelurahan</option>
        <option value="a"></option>
        <option value="B">B</option>
        <option value="c">C</option>
        <option value="d">D</option>
        <option value="e">E</option>
        <option value="f">F</option>
      </select>
    </div>
    <div class="form-group">
      <select class="form-control selectpicker">
        <option value="">Harga</option>
        <option value="a"></option>
        <option value="B">B</option>
        <option value="c">C</option>
        <option value="d">D</option>
        <option value="e">E</option>
        <option value="f">F</option>
      </select>
    </div>
    <div class="form-group">
      <select class="form-control selectpicker">
        <option value="">Luas</option>
        <option value="a"></option>
        <option value="B">B</option>
        <option value="c">C</option>
        <option value="d">D</option>
        <option value="e">E</option>
        <option value="f">F</option>
      </select>
    </div>
    <div class="form-group submit-box">
      <button type="button" class="btn btn-custom  btn-custom-warning"><i class="fa fa-floppy-o"></i>Search</button>
    </div>
  </form>
</div>