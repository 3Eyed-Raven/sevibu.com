<!DOCTYPE html>
<html lang="zxx">
  <head>
    <meta charset="UTF-8" />
    <title>Sevibu.com | @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="icon" href="frontend/img/favicon.ico" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="frontend/libraries/font-awesome/css/font-awesome.min.css" />
    <!--[if lt IE 10]>
    <link rel="stylesheet"  href="frontend/css/ie-fix.css" />
    <![endif]-->
    <!-- Start BOOTSTRAP -->
    <link rel="stylesheet" href="frontend/libraries/tether/dist/css/tether.min.css" />
    <link rel="stylesheet" href="frontend/libraries/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="frontend/css/bootstrap-select.min.css" />
    <link rel="stylesheet" href="frontend/libraries/bootstrap-colorpicker-master/dist/css/bootstrap-colorpicker.min.css" />
    <!-- End Bootstrap -->
    <!-- Start Template files -->
    <link rel="stylesheet" href="frontend/css/world-dealer.css" />
    <link rel="stylesheet" href="frontend/css/world-dealer-media.css" />
    <!-- End  Template files -->
    <!-- Start blueimp  -->
    <link rel="stylesheet" href="frontend/css/blueimp-gallery.min.css" />
    <!-- End blueimp  -->
    <!-- Start custom styles  -->
    <link rel="stylesheet" href="frontend/css/custom.css" />
    <!-- End custom styles  -->
    <script src="frontend/js/modernizr.custom.js"></script>
    <!-- Start codemirror  -->
    <link rel="stylesheet" href="frontend/libraries/codemirror/lib/codemirror.css" />
    <!-- End codemirror  -->
    <link rel="stylesheet" href="frontend/libraries/nouislider/nouislider.css" />
  </head>
  <body>
    <header class="header container container-palette affix-menu">
      @include('frontend.component.header')
    </header>
    <!-- /.header-->
    <div class="widget-top-title-2 container section container-palette">
      <div class="color-secondary"></div>
    </div>
    <main class="container section" style="width: auto;">
      <div class="box-fill box">
        <div class="row">
          <div class="col-sm-3" style="background-color: #0e80b0;">
            <div class="widget widge-search-right">
              @include('frontend.component.sidebar')
            </div>
          </div>
          <div class="col-sm-9">
            @yield('content')
          </div>
        </div>
      </div>
    </main>
    <footer class="footer container container-palette">
      @include('frontend.component.footer')
    </footer><!-- /. footer -->
    <a class="btn btn-scoll-up" id="btn-scroll-up"></a>
    <script src="frontend/js/jquery-2.2.1.min.js"></script>
    <script src="frontend/libraries/jquery.mobile/jquery.mobile.custom.min.js"></script>
    <!-- End Jquery -->
    <!-- Start BOOTSTRAP -->
    <script src="frontend/libraries/tether/dist/js/tether.min.js"></script>
    <script src="frontend/libraries/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="frontend/js/bootstrap-select.min.js"></script>
    <script src="frontend/libraries/bootstrap-colorpicker-master/dist/js/bootstrap-colorpicker.min.js"></script>
    <!-- End Bootstrap -->
    <!-- Start Template files -->
    <script src="frontend/js/world-dealer.js"></script>
    <!-- End  Template files -->
    <!-- Start blueimp  -->
    <script src="frontend/js/blueimp-gallery.min.js" type="text/javascript"></script>
    <!-- End blueimp  -->
    <!-- Start custom styles  -->
    <script src="frontend/js/custom.js" type="text/javascript"></script>
    <!-- End custom styles  -->
    <script src="frontend/js/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="frontend/js/moment-timezone-with-data.js" type="text/javascript"></script>
    <!-- Start kevalbhatt-worldmapgenerator http://kevalbhatt.github.io/WorldMapGenerator/ -->
    <script src="frontend/js/worldmapgenerator_custom.js" type="text/javascript"></script>
    <!-- End kevalbhatt-worldmapgenerator  -->
    <!-- Start codemirror  -->
    <script src="frontend/libraries/codemirror/lib/codemirror.js"></script>
    <script src="frontend/libraries/codemirror/mode/javascript/javascript.js"></script>
    <script src="frontend/libraries/codemirror/addon/edit/matchbrackets.js"></script>
    <script src="frontend/libraries/codemirror/mode/htmlmixed/htmlmixed.js"></script>
    <script src="frontend/libraries/codemirror/mode/xml/xml.js"></script>
    <script src="frontend/libraries/codemirror/mode/clike/clike.js"></script>
    <script src="frontend/libraries/codemirror/mode/php/php.js"></script>
    <script src="frontend/js/codemirror_init.js"></script>
    <!-- End codemirror  -->
    <script src="frontend/libraries/nouislider/nouislider.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="frontend/js/facebook.js"></script>
  </body>
</html>